#ifndef DEMO_H
#define DEMO_H

#include <QWidget>

namespace Ui {
class demo;
}

class demo : public QWidget
{
    Q_OBJECT

public:
    explicit demo(QWidget *parent = nullptr);
    ~demo();

private:
    Ui::demo *ui;
};

#endif // DEMO_H
