#ifndef FALSE_H
#define FALSE_H

#include <QWidget>

namespace Ui {
class false;
}

class false : public QWidget
{
    Q_OBJECT

public:
    explicit false(QWidget *parent = nullptr);
    ~false();

private:
    Ui::false *ui;
};

#endif // FALSE_H
