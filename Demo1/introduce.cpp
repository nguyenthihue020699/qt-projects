#include "introduce.h"
#include "ui_introduce.h"
#include "mainwindow.h"
#include "hightscore.h"
#include <iostream>
#include <QMessageBox>

Introduce::Introduce(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Introduce)
{
    ui->setupUi(this);
}

Introduce::~Introduce()
{
    delete ui;
}

void Introduce::on_pushButton_clicked()
{
    MainWindow *secondForm = new MainWindow();
    this->destroy();
    secondForm->show();
}

void Introduce::on_pushButton_2_clicked()
{
    QMessageBox msgBox;
    msgBox.setText("LUẬT CHƠI: Người chơi phải trả lời 15 câu hỏi với cấp độ từ dễ đến khó, thời gian suy nghĩ của mỗi câu là 30s. \n\nMỗi câu hỏi có một mức tiền thưởng, tăng dần theo thứ tự. Có ba mốc quan trọng là câu số 5, 10 và 15 (mốc TRIỆU PHÚ). \n\nKhi vượt qua các mốc này, họ chắc chắn có được số tiền thưởng tương ứng của các câu hỏi đó.");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.exec();
}

void Introduce::on_pushButton_3_clicked()
{
    HightScore *hightScoreForm = new HightScore();
    this->hide();
    hightScoreForm->show();
}

void Introduce::on_pushButton_4_clicked()
{
    QMessageBox msgBox;
    msgBox.setText("Bạn có thực sự muốn thoát không?");
    msgBox.setIcon(QMessageBox::Question);
    QAbstractButton* pButtonYes = msgBox.addButton(tr("Không"), QMessageBox::YesRole);
    msgBox.addButton(tr("Có"), QMessageBox::NoRole);

    msgBox.exec();

    if (msgBox.clickedButton() != pButtonYes) {
        this->destroy();
    }
}
