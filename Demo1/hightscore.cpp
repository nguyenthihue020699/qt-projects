#include "hightscore.h"
#include "ui_hightscore.h"
#include "introduce.h"
#include <QCloseEvent>
#include <iostream>
#include <QFile>
#include <QTextStream>
#include <QSqlResult>

HightScore::HightScore(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HightScore)
{
    ui->setupUi(this);
   parseHightScoreData();

}

HightScore::~HightScore()
{
    delete ui;
}

void HightScore::closeEvent(QCloseEvent *event)
{
    Introduce *introForm = new Introduce();
    introForm->show();
}

void HightScore::on_btnBack_clicked()
{
    Introduce *introForm = new Introduce();
    this->destroy();
    introForm->show();
}

void HightScore::parseHightScoreData(){
    mydb = QSqlDatabase::addDatabase("QSQLITE");
    mydb.setDatabaseName("C:/sqlite/gamealtp.db");

    if(!mydb.open())
        qDebug() << "Ket noi that bai";
    else
        qDebug() << "Ket noi thanh cong";

    QSqlQuery query;
    QString hightScoreValue;
    query.exec("SELECT * FROM hightscore LIMIT 1");
    while (query.next()) {
        QString fscore = query.value(0).toString();
        hightScoreValue = fscore ;
    }

    ui->lblHightScore->setText(hightScoreValue);
}
