#ifndef INTRODUCE_H
#define INTRODUCE_H

#include <QWidget>

namespace Ui {
class Introduce;
}

class Introduce : public QWidget
{
    Q_OBJECT

public:
    explicit Introduce(QWidget *parent = nullptr);
    ~Introduce();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

private:
    Ui::Introduce *ui;
};

#endif // INTRODUCE_H
