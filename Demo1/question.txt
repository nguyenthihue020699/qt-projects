Mạnh vì..., bạo vì tiền|Cơm|Gạo|Muối|Bạn|2
Thức ăn nào sau đây thuộc nhóm thức ăn chứa nhiều bột đường?|Gạo|Rau|Thịt|Trứng|1
Ngôi chùa được đúc hoàn toàn bằng đồng tại Việt Nam?|Chùa Vàng|Chùa Bái Đính|Chùa Hương|Chùa Đồng|4
Đơn vị đo dung lượng bộ nhớ nào lớn nhất?|MB|GB|KB|TB|4
Hội hát quan họ nào được tổ chức từ 11 - 13 tháng Giêng Âm lịch hàng năm?|Hội Căm Mường|Hội Lim|Hội Chùa Hương|Hội Cầu Ngư|2
Một chốn... quê?|Một|Hai|Ba|Đôi|4
Cây ngay không sợ...?|Chết đứng|Chết nằm|Ngã|Đổ|1
Saturday trong tiếng Anh là thứ mấy trong tuần?|Thứ Ba|Thứ Năm|Thứ Bảy|Chủ Nhật|3
Bán buôn bán lẻ là phạm trù của ngành nào?|Nông nghiệp|Thương nghiệp|Giao thông|Giáo dục|2
Trong các số do dưới đây, số đo nào bằng 25,08 km.|2508m|25080m|250800m|2508000m|2
Người đẹp vì lụa, ... tốt vì phân.|Lúa|Lá|Cây|Hoa|1
Khả năng đặc biệt có thể làm cho người khác bị mê gọi là?|Gọi|Nói chuyện|Hỏi|Thôi miên|4
Miệng đồng ruột thép thẳng băng/Vặn tai miệng nhả nước văng tràn trề.|Cái giếng|Cái bể|Cái vòi nước|Cái chậu nước|3
Trạng thái được coi là trạng thái thứ tư sau rắn lỏng khí.|Hơi|Nguội|Nóng|Plasma|4
Người ta thường gọi quốc gia nào là đất nước mặt trời mọc.|Việt Nam|Nhật Bản|Trung Quốc|Thái Lan|2
Đây là hoạt động người dân bắc bộ làm để ngăn lũ lụt.|Đắp đê|Kè đá|Xây tường|Trồng cây|1
Nóc nhà Đông Dương.|Yên Tử|Phanxipang|Pu si lung|Bạch Mộc Lương Tử|2
Mưa chẳng qua... gió chẳng qua mùi.|Ngọ|Tý|Dần|Mão|1
Vị thần coi giữ đất đai của một khu vực được dân gian gọi là gì?.|Đất công|Đai công|Canh công|Thổ công|4
Bên trên là ngói, bên dưới là hang.|Mũi|Mắt|Miệng|Tai|3
Người Việt Nam đầu tiên bay vào vũ trụ.|Kim Thành|Phạm Tuân|Nguyễn Minh|Hoàng Thanh|2
Tác phẩm bắt đầu bằng tiếng trống thu không.|Lão Hạc|Tắt đèn|Hai đứa trẻ|Chí Phèo|3
Đâu là tên 1 nguyên tố hóa học.|Leti|Lite|Lili|Liti|4
Giao điểm 3 đường trung trực của tam giác gọi là?|Trọng tâm|Trực tâm|Tâm đường tròn nội tiếp|Tâm đường tròn ngoại tiếp|4
Con có cha như nhà có nóc, con không cha như... đứt đuôi.|Nòng nọc|Cá cóc|Cá nheo|Cá lóc|1
Trống đánh thật khỏe, đuốc lóe thật nhanh, quạt khắp xa gần, nước văng tung tóe.|Gió|Bão|Giông|Sấm chớp mưa|4
Ngọn núi nào cao nhất Nhật Bản.|Phanxipang|Phú Sĩ|Yên Tử|Everes|2
Trước Wasington, thành phố nào là thủ đô của Mỹ.|Luanda|Praia|Ottawa|Philadenphia|4
Chị em dâu như bầu...|Nước lã|Nước mát|Nước nóng|Nước lạnh|1
Năm 1910, Morgan đã chọn cái gì làm thí nghiệm về di truyền.|Chuột Bạch|Ruồi giấm|Nhện|Mèo|2
Loại vật liệu dùng trong sản xuất thủy tinh.|Xi măng|Vôi|Cát trắng|Nhựa|3
Ao... nước đọng.|Sâu|Tù|Nông|Lắng|2
Loài động vật nào có 3 tim, 8 chi và máu màu xanh.|Bạch tuộc|Mực|Sứa|Sao|1
Liên đoàn bóng đá Úc thuộc liên đoàn bóng đá nào?|Châu Âu|Châu Phi|Châu Mỹ|Châu Á|4
Thăng Long Hà Nội 1000 tuổi vào năm nào?|2009|2010|2011|2012|2
Đầu nhẹ, bụng nặng, có hình bán nguyệt trơn tru không thể ngã là con gì?|Con mèo|Con gấu|Con lật đật|Con trâu|3
Tim người gồm bao nhiêu ngăn?|1|2|3|4|4
Chùa Đồng lớn nhất Việt Nam ở đâu?|Núi Tà Xùa|Núi Hồng Lĩnh|Núi Yên Tử|Núi Bà Đen|3
U nó không được thế! là kiểu câu gì?|Câu hỏi|Câu nghi vấn|Câu cảm thán|Câu cầu khiến|4
Phương tiện nào sau đây ít giống với những cái còn lại?|Xe đạp|Máy bay|Xe máy|Ô tô|2
Sau chiến tranh thế giới 2, phong trào giải phóng dân tộc nổi lên mạnh nhất ở đâu?|Châu Phi|Châu Âu|Châu Á|Châu Mỹ|1
Công thức hóa học của đá vôi?|CaCO3|Ca(OH)2|CaO|CaSO4|1
Vua nào đặt nhiều niên hiệu nhất lịch sử nước ta.|Bảo Đại|Lý Nhân Tông|Trần Thánh Tông|Trần Nhân Tông|2
Huyện Võ Nhai thuộc tỉnh nào nước ta?|Ninh Bình|Cao Bằng|Thái Nguyên|Lâm Đồng|3
Biển có nồng độ muối lớn nhất thế giới?|Biển chết|Biển đen|Biển đỏ|Biển xanh|1
Ngày bầu cử quốc hội khóa 12?|21-05-2007|20-05-2007|19-05-2007|18-05-2007|2
Quốc kỳ nào giống hệt quốc kỳ Indonexia nhưng ngược chiều.|Thai Lan|Campuchia|Lào|Ba Lan|4
Kinh thành trà kiệu thuộc tỉnh nào?|Quảng Trị|Quảng Nam|Đà Nẵng|Huế|2
