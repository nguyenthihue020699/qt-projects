#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void readQuestionData();
    void randomQuestionData();
    void parseQuestionScreen();
    void initTimeCountDown();
    void changeTimeValue();
    void replayGame();
    void stopGame();
    void saveHightScore();

private slots:
    void on_btnAnswer1_clicked();
    void checkAnswer(qint32);

    void on_btnAnswer2_clicked();

    void on_btnAnswer3_clicked();

    void on_btnAnswer4_clicked();

private:
    Ui::MainWindow *ui;
    void loadScreen();
    QTimer * timer;
    qint32  counter;
    qint32  questionIndex;
    qint32 answerIndex;
    qint32 coreValue;
    QStringList questionArr;
    QStringList questionArrEasy;
    QStringList questionArrMedium;
    QStringList questionArrDificult;
    QSqlDatabase mydb;
    void closeEvent(QCloseEvent *bar);
};
#endif // MAINWINDOW_H
