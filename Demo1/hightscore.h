#ifndef HIGHTSCORE_H
#define HIGHTSCORE_H

#include <QWidget>
#include <QtSql>
#include <QSqlDatabase>

namespace Ui {
class HightScore;
}

class HightScore : public QWidget
{
    Q_OBJECT

public:
    explicit HightScore(QWidget *parent = nullptr);
    ~HightScore();


private slots:
    void on_btnBack_clicked();
    void parseHightScoreData();

private:
    Ui::HightScore *ui;
    QSqlDatabase mydb;
    void closeEvent(QCloseEvent *bar);
};

#endif // HIGHTSCORE_H
