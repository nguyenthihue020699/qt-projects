#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPoint>
#include <QTimer>
#include <QMessageBox>
#include <qDebug>
#include <iostream>
#include <QFile>
#include <QTextStream>
#include <fstream>
#include <QApplication>
#include <QSqlQuery>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initTimeCountDown();
    readQuestionData();
    parseQuestionScreen();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    stopGame();
}

void MainWindow::parseQuestionScreen() {
    randomQuestionData();
    QStringList quesArr = questionArr;
    ui->lblQuestionNumber->setText("Cau hoi so " + QString::number(questionIndex));
    ui->lblQuestion->setText(quesArr[0]);
    ui->btnAnswer1->setText(quesArr[1]);
    ui->btnAnswer2->setText(quesArr[2]);
    ui->btnAnswer3->setText(quesArr[3]);
    ui->btnAnswer4->setText(quesArr[4]);
    answerIndex = quesArr[5].toInt();
    ui->lblCore->setText("$" + QString::number(coreValue));
}

void MainWindow::randomQuestionData() {
    srand (time(NULL));
    if(questionIndex <= 5){
        int numberArr = 16 - (questionIndex - 1);
        int randIndex = rand() % numberArr;
        questionArr = questionArrEasy[randIndex].split("|");
        QStringList quesTemp;
        for (int i = 0; i < numberArr; i++){
            if(i != randIndex){
                quesTemp.append(questionArrEasy[i]);
            }
        }
        questionArrEasy = quesTemp;
    } else if(questionIndex <= 10) {
        int numberArr = 16 - (questionIndex - 5 - 1);
        int randIndex = rand() % numberArr;
        questionArr = questionArrMedium[randIndex].split("|");
        QStringList quesTemp;
        for (int i = 0; i < numberArr; i++){
            if(i != randIndex){
                quesTemp.append(questionArrMedium[i]);
            }
        }
        questionArrMedium = quesTemp;
    } else {
        int numberArr = 16 - (questionIndex - 10 - 1);
        int randIndex = rand() % numberArr;
        questionArr = questionArrDificult[randIndex].split("|");
        QStringList quesTemp;
        for (int i = 0; i < numberArr; i++) {
            if(i != randIndex) {
                quesTemp.append(questionArrDificult[i]);
            }
        }
        questionArrDificult = quesTemp;
    }
}

void MainWindow::readQuestionData() {
    questionArrEasy.clear();
    questionArrMedium.clear();
    questionArrDificult.clear();
    questionIndex = 1;
    coreValue = 0;
    //Reading file
    QFile inputFile(":/new/prefix1/question.txt");

    if (inputFile.open(QIODevice::ReadOnly))
    {

       QTextStream in(&inputFile);
       in.setCodec("UTF-8");
       qint32 i = 0;
       while (!in.atEnd())
       {
          QString line = in.readLine();
          if(i < 16){
              questionArrEasy.append(line);
          } else if(i < 32) {
              questionArrMedium.append(line);
          } else {
              questionArrDificult.append(line);
          }
          i++;
       }
       inputFile.close();
    }

}

void MainWindow::initTimeCountDown(){
    counter = 30;
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()), this ,SLOT(changeTimeValue()));
    timer->start(1000);
}

void MainWindow::changeTimeValue(){
    counter--;
    ui->label_3->setText(QString::number(counter) + 's');
    ui->label_3->setAlignment(Qt::AlignCenter);
    if(counter == 0){
        QMessageBox msgBox;
        msgBox.setText("Hết thời gian. \n\nBạn sẽ dừng cuộc chơi tại đây và ra về với $" + QString::number(coreValue));
        QAbstractButton* pButtonYes = msgBox.addButton(tr("Chơi lại"), QMessageBox::YesRole);
        msgBox.addButton(tr("Thoát"), QMessageBox::NoRole);
        msgBox.exec();

        if (msgBox.clickedButton()==pButtonYes) {
            replayGame();
        }else{
            stopGame();
        }
    }
}

void MainWindow::replayGame(){

    saveHightScore();
    readQuestionData();
    parseQuestionScreen();
    timer->stop();
    initTimeCountDown();
}

void MainWindow::stopGame(){
    saveHightScore();
    timer->stop();
    this->destroy();
}

void MainWindow::checkAnswer(qint32 _quesIndex){
    timer->stop();
    if(answerIndex == _quesIndex){
        if(questionIndex == 15){

            coreValue += 100;
            ui->lblCore->setText("$" + QString::number(coreValue));

            QMessageBox msgBox;
            msgBox.setText("Xin chúc mừng! Bạn là người thắng cuộc.\n\n Bạn sẽ ra về với số tiền là $" + QString::number(coreValue));
            msgBox.setIcon(QMessageBox::Information);
            QAbstractButton* pButtonYes = msgBox.addButton(tr("Chơi lại"), QMessageBox::YesRole);
            msgBox.addButton(tr("Thoát"), QMessageBox::NoRole);
            msgBox.exec();

            if (msgBox.clickedButton()==pButtonYes) {
                replayGame();
            }else{
                stopGame();
            }
        }else{
            coreValue += 100;
            QMessageBox msgBox;
            msgBox.setText("Tra loi dung!");
            msgBox.setIcon(QMessageBox::Information);
            QAbstractButton* pButtonYes = msgBox.addButton(tr("Choi tiep"), QMessageBox::YesRole);
            msgBox.addButton(tr("Thoat"), QMessageBox::NoRole);
            msgBox.exec();

            if (msgBox.clickedButton()==pButtonYes) {
                questionIndex++;
                parseQuestionScreen();
                initTimeCountDown();
            }else{
                stopGame();
            }
        }
    }else{
        QMessageBox msgBox;
        QString falseText = "Bạn đã trả lời sai.\n\nBạn sẽ dừng cuộc chơi tại đây và ra về với $" + QString::number(coreValue);
        msgBox.setText(falseText);
        msgBox.setIcon(QMessageBox::Information);
        QAbstractButton* pButtonYes = msgBox.addButton(tr("Chơi lại"), QMessageBox::YesRole);
        msgBox.addButton(tr("Thoát"), QMessageBox::NoRole);

        msgBox.exec();

        if (msgBox.clickedButton()==pButtonYes) {
            replayGame();
        }else{
            stopGame();
        }
    }
}

void MainWindow::on_btnAnswer1_clicked()
{
    checkAnswer(1);
}

void MainWindow::on_btnAnswer2_clicked()
{
    checkAnswer(2);
}

void MainWindow::on_btnAnswer3_clicked()
{
    checkAnswer(3);
}

void MainWindow::on_btnAnswer4_clicked()
{
    checkAnswer(4);
}

void MainWindow::saveHightScore(){
    mydb = QSqlDatabase::addDatabase("QSQLITE");
    mydb.setDatabaseName("C:/sqlite/gamealtp.db");

    if(!mydb.open())
        qDebug() << "Ket noi that bai";
    else
        qDebug() << "Ket noi thanh cong";

    QSqlQuery query;

    QStringList hightScoreValue;

    query.exec("SELECT * FROM hightscore LIMIT 1");
    while (query.next()) {
        hightScoreValue.append(query.value(0).toString());
    }

    if(coreValue > hightScoreValue[0].toInt()){
        query.exec("UPDATE hightscore SET fscore = " + QString::number(coreValue) + " WHERE fscore = "+ hightScoreValue[0]);
    }

}
